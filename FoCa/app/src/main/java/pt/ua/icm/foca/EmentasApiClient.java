package pt.ua.icm.foca;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by inesg on 03/10/2017.
 */

public class EmentasApiClient {

    /**
     * calls the remote web service and returns the JSON results as a string
     *
     * @param baseURL   the remote resource
     * @return  results enconded in JSON
     * @throws MalformedURLException
     * @throws IOException
     */
    static public String callEmentasWS(String baseURL) throws IOException {
        URL url;
        StringBuilder builder = new StringBuilder();

        url = new URL(baseURL);
        // this code works with API 23
        // for older API, the Apache HttpConnection may be used instead
        // e.g.: http://www.vogella.com/tutorials/AndroidJSON/article.html#androidjson_read
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } finally {
            urlConnection.disconnect();
        }
        return builder.toString();
    }


    /**
     * parses the web service response to instantiate and fill a FoodMenus object
     * @param jsonResults the response from the remote web service
     * @return a navigable collection of daily food options
     * @throws IOException
     */
    static public UAMenus parseJson(String jsonResults) throws IOException {
        UAMenus menusToReturn = new UAMenus();  // return object

        JSONArray menuOptionsArray, jsonArray2;
        JSONObject workingJsonObject;
        DailyOption dailyOption;
        SimpleDateFormat dateParser = new SimpleDateFormat("EEE, dd MMM yyyy");

        try {
            workingJsonObject = new JSONObject(jsonResults);    // access root object
            workingJsonObject = workingJsonObject.getJSONObject("menus"); // access menus within root
            menuOptionsArray = workingJsonObject.getJSONArray("menu"); // options for a zone

            // go through the several entries (day,canteen)
            for (int dailyEntry = 0; dailyEntry < menuOptionsArray.length(); dailyEntry++) {
                dailyOption = new DailyOption(
                        dateParser.parse(menuOptionsArray.getJSONObject(dailyEntry).getJSONObject("@attributes").getString("date")),
                        menuOptionsArray.getJSONObject(dailyEntry).getJSONObject("@attributes").getString("canteen"),
                        menuOptionsArray.getJSONObject(dailyEntry).getJSONObject("@attributes").getString("meal"),
                        menuOptionsArray.getJSONObject(dailyEntry).getJSONObject("@attributes").getString("disabled").compareTo("0") == 0);

                if (dailyOption.isAvailable()) {
                    jsonArray2 = menuOptionsArray.getJSONObject(dailyEntry).getJSONObject("items").getJSONArray("item");
                    // get the several meals in a canteen, in a day
                    dailyOption.addMealCourse(new MealCourse(UAMenus.COURSE_ORDER_SOUP, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_SOUP) ));
                    dailyOption.addMealCourse(new MealCourse(UAMenus.COURSE_ORDER_MEAT_NRM, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_MEAT_NRM) ));
                    dailyOption.addMealCourse(new MealCourse(UAMenus.COURSE_ORDER_FISH_NRM, parseForObjectOrString(jsonArray2, UAMenus.COURSE_ORDER_FISH_NRM) ));
                    //// TODO: complete with more meal courses
                }
                menusToReturn.add(dailyOption);
            }
        } catch (JSONException | ParseException ex) {
            ex.printStackTrace();
        }
        return menusToReturn;
    }



    /**
     * parses the meal course options; the JSON is not coherent, and can be an object or a string
     * @param array array with the meal options
     * @param index position to retrieve
     * @return the meal course option
     * @throws JSONException
     */
    static private String parseForObjectOrString(JSONArray array, int index) throws JSONException {
        JSONObject tempJsonObject = array.optJSONObject(index);
        if( null == tempJsonObject ) {
            // no json object, treat as string
            return array.getString(index);
        } else {
            return array.getJSONObject(index).getJSONObject("@attributes").getString("name");
        }

    }
}
