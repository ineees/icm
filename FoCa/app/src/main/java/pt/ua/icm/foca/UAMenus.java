package pt.ua.icm.foca;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by inesg on 03/10/2017.
 */

public class UAMenus {
    static final public int COURSE_ORDER_SOUP = 0;
    static final public int COURSE_ORDER_MEAT_NRM = 1;
    static final public int COURSE_ORDER_FISH_NRM = 2;

    // the list of options, per day and per canteen
    private static List<DailyOption> dailyMenus = new ArrayList<>();


    public void add(DailyOption dailyOption) {
        this.getDailyMenus().add(dailyOption);
    }


    /**
     * dumps the content of the object into a string for logging/dubbuging
     * @return the contents as String
     */
    @Override
    public String toString() {
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder builder = new StringBuilder();
        for (DailyOption option : this.getDailyMenus()) {
            builder.append("Day: ");
            builder.append(dateFormater.format(option.getDate()));
            builder.append("\tCanteen: ");
            builder.append(option.getCanteenSite());
            builder.append("\tMeal type: ");
            builder.append(option.getDailyMeal());
            builder.append("\tIs open? ");
            builder.append(option.isAvailable());
            builder.append("\n");
            for (MealCourse mealOption: option.getMealCourseList() ) {
                builder.append("\tCourse: ");   builder.append(mealOption.getMealCourse());
                builder.append("\t meal: ");   builder.append(mealOption.getFoodOption());
                builder.append("\n");
            }
        }
        return builder.toString();
    }

    public String getDate() {
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder builder = new StringBuilder();
        for (DailyOption option : this.getDailyMenus()) {
            builder.append("Day: ");
            builder.append(dateFormater.format(option.getDate()));
            builder.append("\tCanteen: ");
            builder.append(option.getCanteenSite());
            builder.append("\n");
        }
        return builder.toString();
    }

    public static String getMenu(int position) {
        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
        DailyOption option = dailyMenus.get(position);
        StringBuilder builder = new StringBuilder();
        builder.append("Day: ");
        builder.append(dateFormater.format(option.getDate()));
        builder.append("\nCanteen: ");
        builder.append(option.getCanteenSite());
        builder.append("\nMeal type: ");
        builder.append(option.getDailyMeal());
        builder.append("\nIs open? ");
        builder.append(option.isAvailable());
        builder.append("\n");
        for (MealCourse mealOption: option.getMealCourseList() ) {
            builder.append("\tCourse: ");   builder.append(mealOption.getMealCourse());
            builder.append("\t meal: ");   builder.append(mealOption.getFoodOption());
            builder.append("\n");
        }

        return builder.toString();
    }

    public List<DailyOption> getDailyMenus() {
        return dailyMenus;
    }
}
