package pt.ua.icm.foca;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by inesg on 03/10/2017.
 */

public class HeadlinesFragment extends ListFragment {
    OnHeadlineSelectedListener mCallback;
    String stringUrl;

    public HeadlinesFragment(String stringUrl) {
        this.stringUrl=stringUrl;
    }

    // Container Activity must implement this interface
    public interface OnHeadlineSelectedListener {
        public void onArticleSelected(int position);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // We need to use a different list item layout for devices older than Honeycomb
        int layout = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
                android.R.layout.simple_list_item_activated_1 : android.R.layout.simple_list_item_1;

        List<String> array = new ArrayList<>();
        array = getArrayDateCanteen();
        setListAdapter(new ArrayAdapter<String>(getActivity(), layout, array));
    }

    @Override
    public void onStart() {
        super.onStart();

        // When in two-pane layout, set the listview to highlight the selected list item
        // (We do this during onStart because at the point the listview is available.)
        if (getFragmentManager().findFragmentById(R.id.article_fragment) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Notify the parent activity of selected item
        mCallback.onArticleSelected(position);

        // Set the item as checked to be highlighted when in two-pane layout
        getListView().setItemChecked(position, true);
    }

    public List<String> getArrayDateCanteen() {
        //this allow to perform network tasks on the UI thread
        // you may use this just for testing, not for production
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        List<String> array = new ArrayList<>();
        try {
            // call the web service
            String response =  EmentasApiClient.callEmentasWS(stringUrl);
            Log.i("EMENTAS", "Got JSON");

            UAMenus menus = EmentasApiClient.parseJson(response);
            Log.i("EMENTAS", "JSON results parsed");

            String days = menus.getDate();
            String[] daysSplit = days.split("\n");
            for (String daysCanteen : daysSplit) {
                String[] daysCanteenSplit = daysCanteen.split("\t");
                array.add(daysCanteenSplit[0]+"\n"+daysCanteenSplit[1]);
            }
            Log.i("EMENTAS", menus.getDate());

        } catch (IOException e) {
            Log.e( "EMENTAS", e.getMessage());
        }
        return array;
    }
}
