package pt.ua.icm.foca;

/**
 * Created by inesg on 03/10/2017.
 */

public class MealCourse {
    private int courseOrder;
    private String foodOption;

    public int getMealCourse() {
        return courseOrder;
    }

    public String getFoodOption() {
        return foodOption;
    }

    public MealCourse(int mealCourseOrder, String foodOption) {
        this.courseOrder = mealCourseOrder;
        this.foodOption = foodOption;
    }
}
